package com.emedinaa.kotlinapp.utils

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * @author edmedina
 */

private val IO_EXECUTOR: Executor = Executors.newSingleThreadExecutor()

fun diskThread(f: () -> Unit) {
    IO_EXECUTOR.execute(f)
}