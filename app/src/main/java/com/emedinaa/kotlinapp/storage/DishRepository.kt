package com.emedinaa.kotlinapp.storage

import com.emedinaa.kotlinapp.model.Dish
import kotlinx.coroutines.flow.Flow

interface DishRepository {

    fun getAll(category:Int,callback:(dishes:List<Dish>)->Unit)
    fun getAllFavorites(category:Int,callback:(dishes:List<Dish>)->Unit)
    fun getAll(callback:(dishes:List<Dish>)->Unit)
    fun getAllFavorites(callback:(dishes:List<Dish>)->Unit)


    suspend fun getAllCategory(category:Int):List<Dish>
    suspend fun getAllCategoryFavorites(category:Int):List<Dish>
    suspend fun getAll():List<Dish>
    suspend fun getAllFavorites():List<Dish>

    fun add(dishes: List<Dish>)
    fun add(dish: Dish,
            callback:()->Unit,
            error:(exception:Exception)->Unit)

    fun searchDishes(query:String):Flow<List<Dish>>

    suspend fun add(dish: Dish)

    fun favorite(dishId:Int, isFavorite:Boolean)
}