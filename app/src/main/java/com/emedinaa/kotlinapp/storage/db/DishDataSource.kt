package com.emedinaa.kotlinapp.storage.db

import com.emedinaa.kotlinapp.model.Dish
import com.emedinaa.kotlinapp.storage.DishRepository
import com.emedinaa.kotlinapp.utils.AppExecutors
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class DishDataSource(
    private val restaurantDataBase: RestaurantDataBase,
    private val appExecutors: AppExecutors
) : DishRepository {

    private val dishDao = restaurantDataBase.dishDao()

    override fun getAll(category: Int, callback: (dish: List<Dish>) -> Unit) {
        appExecutors.diskIO.execute {
            val dishes = dishDao.getAllByCategory(category).map {
                it.toDish()
            }
            appExecutors.mainThread.execute {
                callback(dishes)
            }
        }
    }

    override fun getAllFavorites(category: Int, callback: (dishes: List<Dish>) -> Unit) {
        appExecutors.diskIO.execute {
            val dishes = dishDao.getAllFavoritesByCategory(category).map {
                it.toDish()
            }
            appExecutors.mainThread.execute {
                callback(dishes)
            }
        }
    }

    override fun getAll(callback: (dishes: List<Dish>) -> Unit) {
        appExecutors.diskIO.execute {
            val dishes = dishDao.getAll().map {
                it.toDish()
            }
            appExecutors.mainThread.execute {
                callback(dishes)
            }
        }
    }

    override fun getAllFavorites(callback: (dishes: List<Dish>) -> Unit) {
        appExecutors.diskIO.execute {
            val dishes = dishDao.getAllFavorites().map {
                it.toDish()
            }
            appExecutors.mainThread.execute {
                callback(dishes)
            }
        }
    }

    override suspend fun getAll(): List<Dish> = withContext(Dispatchers.IO) {
        dishDao.getAllWithCoroutines().map {
            it.toDish()
        }
    }

    override suspend fun getAllCategory(category: Int): List<Dish> = withContext(Dispatchers.IO) {
        dishDao.getAllByCategoryCoroutines(category).map {
            it.toDish()
        }
    }

    override suspend fun getAllCategoryFavorites(category: Int): List<Dish> =
        withContext(Dispatchers.IO) {
            dishDao.getAllFavoritesByCategory(category).map {
                it.toDish()
            }
        }

    override suspend fun getAllFavorites(): List<Dish> = withContext(Dispatchers.IO) {
        dishDao.getAllFavoritesCoroutines().map {
            it.toDish()
        }
    }


    override fun searchDishes(query: String): Flow<List<Dish>> {
        return dishDao.dishesByName("%$query%").map { dishes ->
            dishes.map {
                it.toDish()
            }
        }
    }

    override suspend fun add(dish: Dish) {

    }

    override fun add(dishes: List<Dish>) {

    }

    override fun add(dish: Dish, callback: () -> Unit, error: (exception: Exception) -> Unit) {

    }

    override fun favorite(dishId: Int, isFavorite: Boolean) {
        appExecutors.diskIO.execute {
            val favorite = if (isFavorite) 1 else 0
            dishDao.favorite(dishId, favorite)
        }
    }
}