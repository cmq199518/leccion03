package com.emedinaa.kotlinapp.storage.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface CategoryDao {

    @Query("SELECT * FROM tb_category")
    fun getAll():List<CategoryDTO>

    @Insert
    fun insert(categories: List<CategoryDTO>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: CategoryDTO)

    @Update
    fun update(category: CategoryDTO)

    @Delete
    fun delete(category: CategoryDTO)
}