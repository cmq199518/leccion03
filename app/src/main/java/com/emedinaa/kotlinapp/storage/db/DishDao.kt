package com.emedinaa.kotlinapp.storage.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface DishDao {

    @Query("SELECT * FROM tb_dish")
    fun getAll():List<DishDTO>

    @Query("SELECT * FROM tb_dish where category = :category")
    fun getAllByCategory(category:Int):List<DishDTO>

    @Query("SELECT * FROM tb_dish where favorite = 1")
    fun getAllFavorites():List<DishDTO>

    @Query("SELECT * FROM tb_dish where category = :category AND favorite = 1")
    fun getAllFavoritesByCategory(category:Int):List<DishDTO>

    @Query("SELECT * FROM tb_dish")
    suspend fun getAllWithCoroutines():List<DishDTO>

    @Query("SELECT * FROM tb_dish where category = :category")
    suspend fun getAllByCategoryCoroutines(category:Int):List<DishDTO>

    @Query("SELECT * FROM tb_dish where favorite = 1")
    suspend fun getAllFavoritesCoroutines():List<DishDTO>

    @Query("SELECT * FROM tb_dish where category = :category AND favorite = 1")
    suspend fun getAllFavoritesByCategoryCoroutines(category:Int):List<DishDTO>

    @Query("SELECT * FROM tb_dish where name LIKE :name")
    fun dishesByName(name:String=""):Flow<List<DishDTO>>

    @Insert
    fun insert(dishes: List<DishDTO>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(dishDTO: DishDTO)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCoroutines(dishDTO: DishDTO)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(dishDTO: DishDTO)

    @Query("UPDATE tb_dish SET favorite= :favorite WHERE id = :dishId")
    fun favorite(dishId:Int,favorite:Int)

    @Delete
    fun delete(dishDTO: DishDTO)

}