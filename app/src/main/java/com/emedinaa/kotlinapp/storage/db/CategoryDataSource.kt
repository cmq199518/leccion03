package com.emedinaa.kotlinapp.storage.db

import android.util.Log
import com.emedinaa.kotlinapp.model.Category
import com.emedinaa.kotlinapp.storage.CategoryRepository
import com.emedinaa.kotlinapp.utils.AppExecutors
import com.emedinaa.kotlinapp.utils.showCurrentThreadInfo

class CategoryDataSource(
    private val restaurantDataBase: RestaurantDataBase,
    private val appExecutors: AppExecutors
):CategoryRepository {

    private  val categoryDao = restaurantDataBase.categoryDao()

    override fun getAll(callback: (categories: List<Category>) -> Unit) {
        appExecutors.diskIO.execute {
            val categories = categoryDao.getAll().map {
                it.toCategory()
            }
            appExecutors.mainThread.execute {
                 callback(categories)
            }
        }
    }

    override fun add(categories: List<Category>) {
        appExecutors.diskIO.execute {
            categoryDao.insert(categories.map {
                CategoryDTO(it.name,it.desc)
            })
        }
    }

    override fun add(
        category: Category,
        callback: () -> Unit,
        error: (exception: Exception) -> Unit
    ) {
        appExecutors.diskIO.execute {
            Log.v("CONSOLE", showCurrentThreadInfo())
            val categoryDTO = CategoryDTO(category.name, category.desc)
            try {
                //other thread
                categoryDao.insert(categoryDTO)
                appExecutors.mainThread.execute {
                    Log.v("CONSOLE", showCurrentThreadInfo())
                    //main thread
                    callback()
                }
            } catch (exception: Exception) {
                appExecutors.mainThread.execute {
                    Log.v("CONSOLE", showCurrentThreadInfo())
                    //main thread
                    error(exception)
                }
            }
        }
    }

    override fun delete(id: Int, category: Category, callback: () -> Unit) {
        appExecutors.diskIO.execute {
            val categoryDTO = CategoryDTO(category.name, category.desc)
            categoryDTO.id = id
            categoryDao.delete(categoryDTO)
            //main thread
            appExecutors.mainThread.execute {
                callback()
            }
        }
    }

    override fun update(id: Int, category: Category, callback: () -> Unit) {
        appExecutors.diskIO.execute {
            val categoryDTO = CategoryDTO(category.name, category.desc)
            categoryDTO.id = id
            categoryDao.update(categoryDTO)
            //main thread
            appExecutors.mainThread.execute {
                callback()
            }
        }
    }

}