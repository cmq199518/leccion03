package com.emedinaa.kotlinapp.storage

import com.emedinaa.kotlinapp.model.Category

interface CategoryRepository {

    fun getAll(callback:(categories:List<Category>)->Unit)
    fun add(categories: List<Category>)
    fun add(category: Category,
            callback:()->Unit,
            error:(exception:Exception)->Unit)

    fun update(id:Int, category: Category,callback:()->Unit)
    fun delete(id:Int, category: Category,callback:()->Unit)

}