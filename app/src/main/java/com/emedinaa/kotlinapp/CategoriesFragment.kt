package com.emedinaa.kotlinapp

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.emedinaa.kotlinapp.adapters.BaseAdapter
import com.emedinaa.kotlinapp.databinding.FragmentCategoriesBinding
import com.emedinaa.kotlinapp.model.Category
import com.emedinaa.kotlinapp.model.Data
import com.emedinaa.kotlinapp.storage.db.CategoryDataSource
import com.emedinaa.kotlinapp.storage.db.RestaurantDataBase
import com.emedinaa.kotlinapp.utils.AppExecutors

/**
 * @author Eduardo Medina
 */
class CategoriesFragment : Fragment() {

    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!

    private val categoryRepository by lazy {
        CategoryDataSource(RestaurantDataBase.getInstance(requireContext().applicationContext),
            AppExecutors()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoriesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = object : BaseAdapter<Category>(emptyList()) {
            override fun getViewHolder(parent: ViewGroup): BaseViewHolder<Category> {
                val rowView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_category, parent, false)
                return object : BaseAdapter.BaseViewHolder<Category>(rowView) {
                    override fun bind(entity: Category) {
                        rowView.findViewById<TextView>(R.id.textViewName).text = entity.name
                        rowView.findViewById<TextView>(R.id.textViewName).setOnClickListener {
                            onItemSelected(entity)
                        }
                        rowView.findViewById<ImageView>(R.id.imgDelete).setOnClickListener {
                            deleteCategory(entity)
                        }
                        rowView.findViewById<ImageView>(R.id.imgEdit).setOnClickListener {
                            updateCategory(entity)
                        }
                    }
                }
            }
        }

        fetchCategories()
    }

    fun fetchCategories() {
        categoryRepository.getAll {
            (binding.recyclerView.adapter as?  BaseAdapter<Category>)?.update(it)
        }
    }


    private fun updateCategory(category: Category){
        val dialog = CategoryDialog(category)
        dialog.show(parentFragmentManager, "Category")
        fetchCategories()
    }

    private fun deleteCategory(category: Category){
        categoryRepository.delete(category.id,category) {
            fetchCategories()
        }
    }

    private fun onItemSelected(category: Category) {
        findNavController().navigate(
            R.id.action_categoriesFragment_to_dishesFragment2,
            bundleOf(Pair("CATEGORY", category))
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.categories_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.addMenu -> {
                addCategories()
                true
            }
            R.id.refreshList-> {
                fetchCategories()
                true
            }
            R.id.logOutMenu -> {
                exit()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addCategories() {
        /*val categories = listOf(Category(1, "Entradas", "Entradas"),
        Category(2, "Segundos", "Segundos"))
       */
    }

    private fun exit() {
        //findNavController().popBackStack()
        requireActivity().finish()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }




}