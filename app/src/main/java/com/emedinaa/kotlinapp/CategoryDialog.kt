package com.emedinaa.kotlinapp

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.emedinaa.kotlinapp.databinding.DialogCategoryBinding
import com.emedinaa.kotlinapp.model.Category
import com.emedinaa.kotlinapp.storage.db.CategoryDataSource
import com.emedinaa.kotlinapp.storage.db.RestaurantDataBase
import com.emedinaa.kotlinapp.utils.AppExecutors

class CategoryDialog(category: Category) : DialogFragment() {
    private var _binding: DialogCategoryBinding? = null
    private val binding get() = _binding!!
    var category = category

    private val categoryRepository by lazy {
        CategoryDataSource(
            RestaurantDataBase.getInstance(requireContext().applicationContext),
            AppExecutors()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogCategoryBinding.inflate(inflater, container, false)
        binding.cardView.setBackgroundDrawable(null);
        binding.edtNombre.setText(category.name)

        binding.btnConfirm.setOnClickListener {
            category.name = binding.edtNombre.text.toString()
            categoryRepository.update(category.id,category) {
                dismiss()

            }
        }
        return binding.root
    }



}